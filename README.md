# TP Popschool Saint-Omer

## Résumé

Dans ce tuto, nous développerons un site internet utilisé pour gérer un catalogue pour une bibliothèque de quartier.

Dans cette série de tuto, nous verrons comment :

- Utilise l'outil **Express Application Generator** pour créer un squelette de site internet et d'application.
- Démarrer et arrêter le serveur web Node.
- Utiliser une base de données pour stocker les données de votre appli.
- Créer les routes pour requêter les différentes informations et templates (vues) pour effectuer le rendu HTML à afficher dans le navigateur.
- Travailler avec les formulaires.
- Déployer votre appli en production.

A la fin, vous serez capable de créer des petites appli en **Node** avec **Express**.

## Le site internet MaBiliDeQuartier

MaBiliDeQuartier est le nom du site internet que nous créerons et ferons évoluer pendant le tuto.
Comme vous l'avez compris, le but du site internet est de donner accès à un catalogue en ligne pour une petite bibliothèque de quartier,
où les utilisateurs pourront consulter les livres disponibles et gérer leur compte.

Cet exemple a été choisi avec attention parce qu'il présente assez de détails afin de bien comprendre les fonctionnalités d'Express.
Et plus important encore, il nous permet d'avoir un chemin guidé vers toutes les fonctions d'un site internet  :

- Le premier TP vous permettra de créer une simple appli pour consulter les livres disponibles dans la bibliothèque. Cela nous permet de voir les opérations communes à tous les sites internet : lire et afficher des infos depuis une base de données.
- En progressant, nous ajouterons des fonctions plus avancées. Par exemple, nous pourrons ajouter des livres avec des formulaires et permettrons aux utilisateurs de se connecter.

Et ce n'est qu'un début...

## Je suis bloqué, où puis-je trouver le code source ?

Dans le tuto, nous citons les **snippets de code** appropriés à copier/coller et il y aura du code à faire par vous même (avec de l'aide).

Au lieu de copier/coller tous les snippets, essayez de les taper, cela vous permettra de vous familiariser avec le code.

Si vous êtes bloqué, les commits relatifs au passage sont affichés et vous avez donc accès au code à l'instant T.

## Le tuto commence ici

Maintenant que vous en savez un peu plus sur le projet et que ce que vous allez apprendre, c'est le moment de [créer le squelette du projet](/_TUTO/01.Creation d'un squelette de site internet.md).